<?php

namespace App\Http\Controllers;

use App\Models\Signup;
use Illuminate\Http\Request;

use function Ramsey\Uuid\v1;

class TestController extends Controller
{
    public function index()
    {
        return view('test');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'area' => 'required',
            'postcode' => 'required',
            'location' => 'required',
        ]);

        $signup = Signup::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
            'city' => $request->city,
            'area' => $request->area,
            'postcode' => $request->postcode,
            'location' => $request->location,
            'ordernote' => $request->ordernote,
            'date' => $request->date,
            'time' => $request->time,
            'room' => $request->room,
            'bathroom' => $request->bathroom,
            'kitchen' => $request->kitchen,
            'fridge' => $request->fridge,
            'garden' => $request->garden,
            'totalprice' => $request->totalprice,

        ]);

        return view('success');
    }
}
