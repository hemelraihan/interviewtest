<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signups', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('city');
            $table->string('area');
            $table->string('address');
            $table->string('postcode');
            $table->string('ordernote')->nullable();
            $table->string('location');
            $table->string('date');
            $table->string('time');
            $table->string('room');
            $table->string('bathroom');
            $table->string('kitchen');
            $table->string('fridge');
            $table->string('garden');
            $table->string('totalprice');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signups');
    }
}
