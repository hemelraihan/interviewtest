<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>QIXER - Service Based Business HTML Template</title>

    <!-- favicon -->
    <link rel=icon href="favicons.ico" sizes="16x16" type="image/icon">
    <!-- animate -->
    <link rel="stylesheet" href="assets/css/animate.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- LineAwesome -->
    <link rel="stylesheet" href="assets/css/line-awesome.min.css">
    <!-- fontawesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <!-- Nice Select -->
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="assets/css/style.css">

</head>

<body>
    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="loader">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
    <!-- preloader area end -->
    <!-- Header area Starts -->
    <header class="header-style-01">
        <!-- Menu area Starts -->
        <nav class="navbar navbar-area navbar-two navbar-expand-lg navbar-bg-1">
            <div class="container container-two nav-container">
                <div class="responsive-mobile-menu">
                    <div class="logo-wrapper">
                        <a href="multistep_form.html" class="logo">
                            <img src="assets/img/logo-02.png" alt="">
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bizcoxx_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bizcoxx_main_menu">
                    <ul class="navbar-nav">
                        <li><a href="#0">Home</a></li>
                        <li><a href="#0">About</a></li>
                        <li><a href="#0">Service List</a></li>
                        <li><a href="#0">Dashboard</a></li>
                        <li><a href="multistep_form.html">multistep Form</a></li>
                        <li><a href="#0">Blog</a></li>
                        <li><a href="#0">contact</a></li>
                    </ul>
                </div>
                <div class="nav-right-content">
                    <ul>
                        <li>
                            <a href="#">
                                <div class="info-bar-item">
                                    <div class="cart-icon icon">
                                        <i class="las la-shopping-cart"></i>
                                        <div class="cart-list style-02">
                                            <span class="single-list"> Cart One </span>
                                            <span class="single-list"> Cart Two </span>
                                            <span class="single-list"> Cart Three </span>
                                            <span class="single-list"> Cart Four </span>
                                            <span class="single-list"> Cart Five </span>
                                        </div>
                                    </div>
                                    <div class="notification-icon icon">
                                        <i class="las la-bell"></i>
                                        <span class="notification-number style-02"> 4 </span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="info-bar-item-two">
                                    <div class="author-thumb">
                                        <img src="assets/img/author-nav.jpg" alt="">
                                    </div>
                                    <div class="author-nav-content">
                                        <span class="title"> Alex Jerin </span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Menu area end -->
    </header>
    <!-- Header area end -->
    <!-- Location Overview area starts -->
    <section class="location-overview-area padding-top-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <form action="{{ route('store') }}" method="POST" id="msform" class="msform">
                        @csrf
                        <ul class="overview-list step-list">
                            <li class="list active" id="account">
                                <a class="list-click" href="javascript:void(0)"> <span class="list-number">1</span> Location </a>
                            </li>
                            <li class="list">
                                <a class="list-click" href="javascript:void(0)"> <span class="list-number">2</span> Service </a>
                            </li>
                            <li class="list">
                                <a class="list-click" href="javascript:void(0)"> <span class="list-number">3</span> Date & Time </a>
                            </li>
                            <li class="list">
                                <a class="list-click" href="javascript:void(0)"> <span class="list-number">4</span> Information </a>
                            </li>
                            <li class="list">
                                <a class="list-click" href="javascript:void(0)"> <span class="list-number">5</span> Confirmation </a>
                            </li>
                        </ul>
                        <!-- Location -->
                        <fieldset class="padding-top-50 padding-bottom-100">
                            <div class="overview-list-all">
                                <div class="overview-location">
                                    <div class="loc1 single-location active margin-top-30">
                                        <span id="location1" class="location"> New York, </span>
                                        <input id="total" type="hidden" name="location" value="">
                                    </div>
                                    <div class="loc2 single-location margin-top-30" id="loc2" onclick="makeActive()">
                                        <span id="location2" class="location"> Los Angeles, </span>
                                        {{-- <input id="total2" type="hidden" name="total" value=""> --}}
                                    </div>
                                    <div class="loc3 single-location margin-top-30" id="loc3" onclick="makeActive2()">
                                        <span id="location3" class="location"> Chicago, </span>
                                        {{-- <input id="total3" type="hidden" name="total" value=""> --}}
                                    </div>
                                    <div class="loc4 single-location margin-top-30" id="loc4" onclick="makeActive3()">
                                        <span id="location4" class="location"> Houston, </span>
                                        {{-- <input id="total4" type="hidden" name="total" value=""> --}}
                                    </div>
                                    <div class="loc5 single-location margin-top-30" id="loc5" onclick="makeActive4()">
                                        <span id="location5" class="location"> Phoenix, </span>
                                        {{-- <input id="total5" type="hidden" name="total" value=""> --}}
                                    </div>
                                </div>
                            </div>
                            <!-- Contact Map Starts -->
                            <div class="contact-map-area padding-top-100">
                                <div class="container">
                                    <div class="contact-map">
                                        <span class="select-location"> Select Current Location </span>
                                        <iframe src="https://maps.google.com/maps?q=23.8283,90.4115&hl=es;z=14&amp;output=embed"></iframe>
                                    </div>
                                </div>
                            </div>
                            <!-- Contact Map end -->
                            <input type="button" name="next" onclick="purchase()" class="next action-button" value="Next" />
                        </fieldset>
                        <!-- Service -->
                        <fieldset class="padding-top-50 padding-bottom-100">
                            <div class="row">
                                <div class="col-lg-8 margin-top-30">
                                    <div class="service-overview-wrapper padding-bottom-30">
                                        <div class="overview-author overview-author-border">
                                            <div class="overview-flex-author">
                                                <div class="overview-thumb">
                                                    <img src="assets/img/service/overview1.jpg" alt="">
                                                </div>
                                                <div class="overview-contents">
                                                    <h4 class="overview-title"> <a href="javascript:void(0)"> Lorem ipsum dolor sit amet, consectetur adipiscing about Aelit</a> </h4>
                                                    <span class="overview-review"> <i class="las la-star"></i> 4.9 <b>(231)</b> </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="overview-single padding-top-40">
                                            <h4 class="title"> What's Included </h4>
                                            <div class="include-contents margin-top-30">
                                                <div class="single-include">
                                                    <ul class="include-list">
                                                        <li class="lists">
                                                            <div class="list-single">
                                                                <span class="rooms"> <span id="roomnum">3</span> Bed Room </span>
                                                            </div>
                                                            <div class="list-single">
                                                                <span class="values"> $30 </span>
                                                                <span class="value-input"> <input type="text" name="room" id="inputroom" oninput="roomNum()" value="3"> </span>
                                                            </div>
                                                        </li>
                                                        <li class="lists"> <a class="remove" href="javascript:void(0)">Remove</a> </li>
                                                    </ul>
                                                </div>
                                                <div class="single-include">
                                                    <ul class="include-list">
                                                        <li class="lists">
                                                            <div class="list-single">
                                                                <span class="rooms"> <span id="bathroomnum">2</span> Bath Room </span>
                                                            </div>
                                                            <div class="list-single">
                                                                <span class="values"> $20 </span>
                                                                <span class="value-input"> <input type="text" id="inputbath" name="bathroom" oninput="bathroomNum()" value="2"> </span>
                                                            </div>
                                                        </li>
                                                        <li class="lists"> <a class="remove" href="javascript:void(0)">Remove</a> </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="overview-single padding-top-60">
                                            <h4 class="title"> Upgrade your order with extras </h4>
                                            <div class="row">
                                                <div class="col-lg-6 margin-top-30">
                                                    <div class="overview-extra">
                                                        <div class="checkbox-inlines">
                                                            <input class="check-input" type="checkbox" id="check1">
                                                            <label class="checkbox-label" for="check1"> Kitchen Cleaning </label>
                                                        </div>
                                                        <div class="overview-extra-flex-content">
                                                            <div class="list-single">
                                                                <span class="values"> $50 </span>
                                                                <span class="value-input"> <input type="text" id="inputkitchen" name="kitchen" oninput="kitchenNum()" value="3"> </span>
                                                            </div>
                                                            <span class="price-value"> $50 </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 margin-top-30">
                                                    <div class="overview-extra">
                                                        <div class="checkbox-inlines">
                                                            <input class="check-input" type="checkbox" id="check2">
                                                            <label class="checkbox-label" for="check2"> Fridge </label>
                                                        </div>
                                                        <div class="overview-extra-flex-content">
                                                            <div class="list-single">
                                                                <span class="values"> $50 </span>
                                                                <span class="value-input"> <input type="text" id="inputfridge" name="fridge" oninput="fridgeNum()" value="3"> </span>
                                                            </div>
                                                            <span class="price-value"> $50 </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 margin-top-30">
                                                    <div class="overview-extra">
                                                        <div class="checkbox-inlines">
                                                            <input class="check-input" type="checkbox" id="check3">
                                                            <label class="checkbox-label" for="check6"> Garden Cleaning </label>
                                                        </div>
                                                        <div class="overview-extra-flex-content">
                                                            <div class="list-single">
                                                                <span class="values"> $50 </span>
                                                                <span class="value-input"> <input type="text" id="inputgarden" name="garden" oninput="gardenNum()" value="3"> </span>
                                                            </div>
                                                            <span class="price-value"> $50 </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <div class="col-lg-6 margin-top-30">
                                                    <div class="overview-extra">
                                                        <div class="checkbox-inlines">
                                                            <input class="check-input" type="checkbox" id="check7">
                                                            <label class="checkbox-label" for="check7"> Kitchen Cleaning </label>
                                                        </div>
                                                        <div class="overview-extra-flex-content">
                                                            <div class="list-single">
                                                                <span class="values"> $25 </span>
                                                                <span class="value-input"> <input type="text" value="3"> </span>
                                                            </div>
                                                            <span class="price-value"> $50 </span>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                        <div class="overview-single padding-top-60">
                                            <h4 class="title"> Benifits of the Package: </h4>
                                            <ul class="overview-benefits margin-top-30">
                                                <li class="list"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a egestas leo. </li>
                                                <li class="list"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a egestas leo. </li>
                                                <li class="list"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed a egestas leo. </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 margin-top-30">
                                    <div class="service-overview-summery">
                                        <h4 class="title"> Booking Summery </h4>
                                        <div class="overview-summery-contents">
                                            <div class="single-summery">
                                                <span class="summery-title"> Appointment Package Service </span>
                                                <div class="summery-list-all">
                                                    <ul class="summery-list">
                                                        <li class="list">
                                                            <span class="rooms"> Bed Room</span>
                                                            <span id="roomcount" class="room-count">3</span>
                                                            <span id="valuecountroom" class="value-count">90</span>
                                                        </li>
                                                        <li class="list">
                                                            <span class="rooms"> Bath Room</span>
                                                            <span id="bathcount" class="room-count">2</span>
                                                            <span id="valuecountbath" class="value-count">60</span>
                                                        </li>
                                                    </ul>
                                                    <ul class="summery-result-list">
                                                        <li class="result-list">
                                                            <span class="rooms"> Pagckage Fee</span>
                                                            <span id="valuecountall" class="value-count">$150</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-summery">
                                                <span class="summery-title"> Extra Service </span>
                                                <div class="summery-list-all">
                                                    <ul class="summery-list">
                                                        <li class="list">
                                                            <span class="rooms"> Kitchen</span>
                                                            <span id="kitchencount" class="room-count">1</span>
                                                            <span id="valuecountkitchen" class="value-count">50</span>
                                                        </li>
                                                        <li class="list">
                                                            <span class="rooms"> Fridge</span>
                                                            <span id="fridgecount" class="room-count">1</span>
                                                            <span id="valuecountfridge" class="value-count">50</span>
                                                        </li>
                                                        <li class="list">
                                                            <span class="rooms"> Garden</span>
                                                            <span id="gardencount" class="room-count">1</span>
                                                            <span id="valuecountgarden" class="value-count">50</span>
                                                        </li>
                                                    </ul>
                                                    <ul class="summery-result-list result-border padding-bottom-20">
                                                        <li class="result-list">
                                                            <span class="rooms"> Subtotal</span>
                                                            <span id="valuecountsubtotal" class="value-count">$280</span>
                                                        </li>
                                                    </ul>
                                                    <ul class="summery-result-list result-border padding-bottom-20">
                                                        <li class="result-list">
                                                            <span class="rooms"> Tax(+)15%</span>
                                                            <span id="tax" class="value-count">42</span>
                                                        </li>
                                                    </ul>
                                                    <ul class="summery-result-list">
                                                        <li class="result-list">
                                                            <span class="rooms"> <strong>Total</strong></span>
                                                            <span id="grandtotal" class="value-count">$280</span>
                                                            <input id="totalprice" type="hidden" name="totalprice" value="">
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="btn-wrapper">
                                                <a href="javascript:void(0)" class="cmn-btn btn-appoinment btn-bg-1"> Continue </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        </fieldset>
                        <!-- Date & Time -->
                        <fieldset class="padding-top-50 padding-bottom-100">
                            <div class="date-overview">
                                <div class="single-date-overview margin-top-30">
                                    <h4 class="date-time-title"> Available on December 20201 </h4>
                                    <ul class="date-time-list margin-top-20">
                                        <li id="dateselect" class="dateselect list active"> <a id="selectdate" href="javascript:void(0)"> 05 dec,2021 - Sun </a> </li>
                                        <li onclick="selectDate1()" id="dateselect1" class="dateselect1 list"> <a id="selectdate1" href="javascript:void(0)"> 06 dec,2021 - Sun </a> </li>
                                        <input id="date" type="hidden" name="date" value="">
                                        <li onclick="selectDate2()" id="dateselect2" class="dateselect2 list"> <a id="selectdate2" href="javascript:void(0)"> 07 dec,2021 - Sun </a> </li>
                                        <li onclick="selectDate3()" id="dateselect3" class="dateselect3 list"> <a id="selectdate3" href="javascript:void(0)"> 08 dec,2021 - Sun </a> </li>
                                        <li onclick="selectDate4()" id="dateselect4" class="dateselect4 list"> <a id="selectdate4" href="javascript:void(0)"> 09 dec,2021 - Sun </a> </li>
                                    </ul>
                                </div>
                                <div class="single-date-overview margin-top-30">
                                    <h4 class="date-time-title"> Available schedule on 06 September, 2021 </h4>
                                    <ul class="date-time-list margin-top-20">
                                        <li id="timeselect" class="timeselect list active"> <a id="selecttime" href="javascript:void(0)"> 10.00AM-11.00AM </a> </li>
                                        <input id="time" type="hidden" name="time" value="">
                                        <li onclick="selectTime1()" id="timeselect1" class="timeselect1 list"> <a id="selecttime1" href="javascript:void(0)"> 12.00AM-01.00PM </a> </li>
                                        <li onclick="selectTime2()" id="timeselect2" class="timeselect2 list"> <a id="selecttime2" href="javascript:void(0)"> 04.00AM-05.00AM </a> </li>
                                        <li onclick="selectTime3()" id="timeselect3" class="timeselect3 list"> <a id="selecttime3" href="javascript:void(0)"> 06.00AM-07.00AM </a> </li>
                                    </ul>
                                </div>
                                <div class="btn-wrapper margin-top-30">
                                    <a href="javascript:void(0)" class="cmn-btn btn-bg-1">Book Appoinmnent</a>
                                </div>
                            </div>
                            <input type="button" name="next" class="next action-button" value="Next" /> <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        </fieldset>
                        <!-- Information -->
                        <fieldset class="padding-top-50 padding-bottom-100">
                            <div class="Info-overview padding-top-30">
                                <h3 class="date-time-title"> Booking Information </h3>
                                <div class="single-info-overview margin-top-30">
                                    <div class="single-info-input">
                                        <label class="info-title"> Your Name* </label>
                                        <input class="form--control @error('name') is-invalid @enderror" type="text" id="yourname" name="name" placeholder="Type Your Name">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="single-info-input">
                                        <label class="info-title"> Your Email* </label>
                                        <input class="form--control @error('email') is-invalid @enderror" type="email" id="email" name="email" placeholder="Type Your Email">
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="single-info-overview margin-top-30">
                                    <div class="single-info-input">
                                        <label class="info-title"> Phone Number* </label>
                                        <input class="form--control @error('phone') is-invalid @enderror" type="tel" id="phone" name="phone" placeholder="Type Your Number">
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="single-info-input">
                                        <label class="info-title"> Your City* </label>
                                        <select name="city" id="city">
                                            <option value="New York">New York</option>
                                            <option value="London">London</option>
                                            <option value="Eden Garden">Eden Garden</option>
                                            <option value="Paris">Paris</option>
                                            <option value="Barcelona">Barcelona</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="single-info-overview margin-top-30">
                                    <div class="single-info-input">
                                        <label class="info-title @error('area') is-invalid @enderror"> Your Area* </label>
                                        <input class="form--control" id="area" name="area" type="text" placeholder="Type Your Area">
                                        @error('area')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                    <div class="single-info-input">
                                        <label class="info-title"> Post Code* </label>
                                        <input class="form--control @error('postcode') is-invalid @enderror" id="postcode" name="postcode" type="tel" placeholder="Type Post Code">
                                        @error('postcode')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="single-info-overview margin-top-30">
                                    <div class="single-info-input">
                                        <label class="info-title"> Your Address* </label>
                                        <input class="form--control @error('address') is-invalid @enderror" id="address" type="text" name="address" placeholder="Type Your Address">
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="single-info-overview margin-top-30">
                                    <div class="single-info-input">
                                        <label class="info-title"> Order Note* </label>
                                        <textarea class="form--control textarea--form" id="ordernote" name="ordernote" placeholder="Type Order Note"></textarea>
                                    </div>
                                </div>
                                <div class="btn-wrapper margin-top-35">
                                    <a href="javascript:void(0)" class="cmn-btn btn-bg-1">Continue</a>
                                </div>
                            </div>
                            <input type="button" onclick="confirmation()" name="next" class="next action-button" value="Next" /> 
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        </fieldset>
                        <!-- Confirmation -->
                        <fieldset class="padding-top-50 padding-bottom-100">
                            <div class="confirm-overview padding-top-30">
                                <div class="overview-author overview-author-border">
                                    <div class="overview-flex-author">
                                        <div class="overview-thumb confirm-thumb">
                                            <img src="assets/img/service/overview1.jpg" alt="">
                                        </div>
                                        <div class="overview-contents">
                                            <h2 class="overview-title confirm-title"> <a href="javascript:void(0)">Lorem ipsum dolor sit amet, consectetur adipiscing about Aelit</a> </h2>
                                            <span class="overview-review"> <i class="las la-star"></i> 4.9 <b>(231)</b> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="confirm-overview-left margin-top-30">
                                        <div class="single-confirm-overview">
                                            <div class="single-confirm margin-top-30">
                                                <h3 class="titles"> Location </h3>
                                                <span id="confirmlocation" class="details"> Los Angeles, </span>
                                            </div>
                                            <div class="single-confirm margin-top-30">
                                                <h3 class="titles"> Date & Time </h3>
                                                <span id="confirmdate" class="details"> 06 Sep 2021 </span>
                                                <span id="confirmtime" class="details"> 6:00PM-7:00PM </span>
                                            </div>
                                        </div>
                                        <div class="booking-info padding-top-60">
                                            <h2 class="title"> Booking Information </h2>
                                            <div class="booking-details">
                                                <ul class="booking-list">
                                                    <li class="lists">
                                                        <span class="list-span"> Name: </span>
                                                        <span id="confirmyourname" class="list-strong"> Jhon Dhoe </span>
                                                    </li>
                                                    <li class="lists">
                                                        <span class="list-span"> Email: </span>
                                                        <span id="confirmemail" class="list-strong"> Customer@mail.com </span>
                                                    </li>
                                                    <li class="lists">
                                                        <span class="list-span"> Phone: </span>
                                                        <span id="confirmphone" class="list-strong"> 011-2234567890 </span>
                                                    </li>
                                                    <li class="lists">
                                                        <span class="list-span"> City: </span>
                                                        <span id="confirmcity" class="list-strong"> New york </span>
                                                    </li>
                                                    <li class="lists">
                                                        <span class="list-span"> Area: </span>
                                                        <span id="confirmarea" class="list-strong"> New york </span>
                                                    </li>
                                                    <li class="lists">
                                                        <span class="list-span"> Post Code: </span>
                                                        <span id="confirmpostcode" class="list-strong"> 1122 </span>
                                                    </li>
                                                    <li class="lists">
                                                        <span class="list-span"> Address: </span>
                                                        <span id="confirmaddress" class="list-strong"> 2608 Ritter Avenue Livonia, MI 48150 </span>
                                                    </li>
                                                    <li class="lists">
                                                        <span class="list-span"> Order Note: </span>
                                                        <span id="confirmordernote" class="list-strong"> it is a long established fact that a reader will be distra 
                                                            by the readable content of a page when looking at its 
                                                            layout. The point of using Lorem Ipsum is that. </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 margin-top-60">
                                    <div class="service-overview-summery">
                                        <h4 class="title"> Booking Summery </h4>
                                        <div class="overview-summery-contents">
                                            <div class="single-summery">
                                                <span class="summery-title"> Appointment Service </span>
                                                <div class="summery-list-all">
                                                    <ul class="summery-list">
                                                        <li class="list">
                                                            <span class="rooms"> Bed Room</span>
                                                            <span id="confirmroomcount" class="room-count">3</span>
                                                            <span id="confirmvaluecountroom" class="value-count">$90</span>
                                                        </li>
                                                        <li class="list">
                                                            <span class="rooms"> Bath Room</span>
                                                            <span id="confirmbathcount" class="room-count">2</span>
                                                            <span id="confirmvaluecountbath" class="value-count">$60</span>
                                                        </li>
                                                    </ul>
                                                    <ul class="summery-result-list">
                                                        <li class="result-list">
                                                            <span class="rooms"> Appoinment Fee</span>
                                                            <span id="confirmvaluecountall" class="value-count">$150</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="single-summery">
                                                <span class="summery-title"> Extra Service </span>
                                                <div class="summery-list-all">
                                                    <ul class="summery-list">
                                                        <li class="list">
                                                            <span class="rooms"> Kitchen</span>
                                                            <span id="confirmkitchencount" class="room-count">1</span>
                                                            <span id="confirmvaluecountkitchen" class="value-count">$50</span>
                                                        </li>
                                                        <li class="list">
                                                            <span class="rooms"> Fridge</span>
                                                            <span id="confirmfridgecount" class="room-count">1</span>
                                                            <span id="confirmvaluecountfridge" class="value-count">$20</span>
                                                        </li>
                                                        <li class="list">
                                                            <span class="rooms"> Garden</span>
                                                            <span id="confirmgardencount" class="room-count">1</span>
                                                            <span id="confirmvaluecountgarden" class="value-count">$60</span>
                                                        </li>
                                                    </ul>
                                                    <ul class="summery-result-list result-border padding-bottom-20">
                                                        <li class="result-list">
                                                            <span class="rooms"> Subtotal</span>
                                                            <span id="confirmvaluecountsubtotal" class="value-count">$280</span>
                                                        </li>
                                                    </ul>
                                                    <ul class="summery-result-list result-border padding-bottom-20">
                                                        <li class="result-list">
                                                            <span class="rooms"> Tax(+)15%</span>
                                                            <span id="confirmtax" class="value-count">$42</span>
                                                        </li>
                                                    </ul>
                                                    <ul class="summery-result-list">
                                                        <li class="result-list">
                                                            <span class="rooms"> <strong>Total</strong></span>
                                                            <span id="confirmgrandtotal" class="value-count">$280</span>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="confirm-bottom-content">
                                                <div class="confirm-payment payment-border">
                                                    <div class="single-checkbox">
                                                        <div class="checkbox-inlines">
                                                            <input class="check-input" type="checkbox" id="check1">
                                                            <label class="checkbox-label" for="check1"> Cash Payment </label>
                                                        </div>
                                                    </div>
                                                    <div class="single-checkbox">
                                                        <div class="checkbox-inlines">
                                                            <input class="check-input" type="checkbox" id="check2">
                                                            <label class="checkbox-label" for="check2"> <img src="assets/img/service/payment.png" alt=""> </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="checkbox-inlines bottom-checkbox">
                                                    <input class="check-input" type="checkbox" id="check3">
                                                    <label class="checkbox-label" for="check3"> I have read and agree to the website <a href="javascript:void(0)"> terms and 
                                                        conditions * </a> </label>
                                                </div>
                                            </div>
                                            <div class="btn-wrapper">
                                                <a href="javascript:void(0)" class="cmn-btn btn-appoinment btn-bg-1"> Pay & Confirm Your Order </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="action-button">Submit</button>  
                            <input type="button" name="previous" class="previous action-button-previous" value="Previous" />
                        </fieldset>
                        <!-- Successful Complete -->
                        {{-- <fieldset class="padding-top-80 padding-bottom-100">
                            <div class="form-card successful-card">
                                <h2 class="title-step"> SUCCESS ! </h2>
                                <div class="succcess-icon">
                                    <i class="las la-check"></i>
                                </div>
                                <h5 class="purple-text text-center">You Have Successfully Signed Up</h5>
                                <div class="btn-wrapper text-center margin-top-35">
                                    <a href="multistep_form.html" class="cmn-btn btn-bg-1"> Back To Home</a>
                                </div>
                            </div>
                        </fieldset> --}}
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- Location Overview area end -->
    <!-- footer area start -->
    <footer class="footer-area section-bg-2">
        <div class="footer-top padding-top-100 padding-bottom-70">
            <div class="container container-two">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget widget style-02">
                            <div class="about_us_widget">
                                <a href="multistep_form.html" class="footer-logo"> <img src="assets/img/logo-02.png" alt="footer logo"></a>
                            </div>
                            <div class="footer-inner">
                                <p class="footer-para">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget widget style-02">
                            <h6 class="widget-title">Category</h6>
                            <div class="footer-inner">
                                <ul class="footer-link-list">
                                    <li class="list"><a href="#">Cleaning</a></li>
                                    <li class="list"><a href="#">House Move</a></li>
                                    <li class="list"><a href="#">Electric</a></li>
                                    <li class="list"><a href="#">Painting</a></li>
                                    <li class="list"><a href="#">Salon & Spa</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget widget style-02">
                            <h6 class="widget-title">Store Info</h6>
                            <div class="footer-inner">
                                <ul class="footer-link-address">
                                    <li class="list"><span class="address"> <i class="las la-map-marker-alt"></i> 41/1, Hilton Mall, NY City </span></li>
                                    <li class="list"> <span class="address"> <a href="tel:+012-78901234"> <i class="las la-mobile-alt"></i> +012-78901234</a> </span></li>
                                    <li class="list"> <span class="address"> <a href="mailto:help@mail.com"> <i class="las la-envelope"></i> help@mail.com</a> </span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget widget style-02">
                            <h6 class="widget-title">Get In Touch</h6>
                            <div class="footer-inner">
                                <p class="subscribe-para"> Sign up to our mailing list now! </p>
                                <form class="subscribe-form" action="#">
                                    <div class="widget-form-single">
                                        <input class="form--control" type="text" name="email" placeholder="Your mail here">
                                        <button type="submit"> <i class="lab la-telegram-plane"></i> </button>
                                    </div>
                                </form>
                                <div class="footer-socials style-02">
                                    <ul class="footer-social-list">
                                        <li class="lists">
                                            <a class="facebook" href="#0"> <i class="lab la-facebook-f"></i> </a>
                                        </li>
                                        <li class="lists">
                                            <a class="twitter" href="#0"> <i class="lab la-twitter"></i> </a>
                                        </li>
                                        <li class="lists">
                                            <a class="instagram" href="#0"> <i class="lab la-instagram"></i> </a>
                                        </li>
                                        <li class="lists">
                                            <a class="youtube" href="#0"> <i class="lab la-youtube"></i> </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-area style-02 copyright-border">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-6">
                        <ul class="copyright-list">
                            <li class="list">
                                <a href="#0"> Privacy Policy </a>
                            </li>
                            <li class="list">
                                <a href="#0"> Terms & Conditions </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="copyright-contents">
                            <span> All copyright (C) 2021 Reserved </span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="copyright-payment">
                            <ul class="payment-list">
                                <li class="list">
                                    <a href="#0"> <img src="assets/img/footer/c1.png" alt=""> </a>
                                </li>
                                <li class="list">
                                    <a href="#0"> <img src="assets/img/footer/c2.png" alt=""> </a>
                                </li>
                                <li class="list">
                                    <a href="#0"> <img src="assets/img/footer/c3.png" alt=""> </a>
                                </li>
                                <li class="list">
                                    <a href="#0"> <img src="assets/img/footer/c4.png" alt=""> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer area end -->

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->



    <!-- jquery -->
    <script src="assets/js/jquery-3.6.0.min.js"></script>
    <!-- jquery Migrate -->
    <script src="assets/js/jquery-migrate-1.4.1.min.js"></script>
    <!-- bootstrap -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- wow -->
    <script src="assets/js/wow.min.js"></script>
    <!-- Nice Select -->
    <script src="assets/js/jquery.nice-select.js"></script>
    <!-- main js -->
    <script src="assets/js/main.js"></script>

</body>

{{-- <script>
    $(document).ready(function(){
        var room = $("#valuecountroom");
        var bath = $("#valuecountbath");
        var roomprice = parseInt($room.html());
        var bathprice = parseInt($bath.html());

        var total = roomprice+bathprice;
        alert(total);
        
});
</script> --}}

<script>

    function confirmation()
    {
        var location = document.getElementById("total").value;
        document.getElementById("confirmlocation").innerText = location;

        var date = document.getElementById("date").value;
        document.getElementById("confirmdate").innerText = date;

        var time = document.getElementById("time").value;
        document.getElementById("confirmtime").innerText = time;

        document.getElementById("confirmroomcount").innerText = document.getElementById("roomcount").innerText;
        document.getElementById("confirmvaluecountroom").innerText = document.getElementById("valuecountroom").innerText;

        document.getElementById("confirmbathcount").innerText = document.getElementById("bathcount").innerText;
        document.getElementById("confirmvaluecountbath").innerText = document.getElementById("valuecountbath").innerText;

        document.getElementById("confirmvaluecountall").innerText = document.getElementById("valuecountall").innerText;

        document.getElementById("confirmkitchencount").innerText = document.getElementById("kitchencount").innerText;
        document.getElementById("confirmvaluecountkitchen").innerText = document.getElementById("valuecountkitchen").innerText;

        document.getElementById("confirmfridgecount").innerText = document.getElementById("fridgecount").innerText;
        document.getElementById("confirmvaluecountfridge").innerText = document.getElementById("valuecountfridge").innerText;

        document.getElementById("confirmgardencount").innerText = document.getElementById("gardencount").innerText;
        document.getElementById("confirmvaluecountgarden").innerText = document.getElementById("valuecountgarden").innerText;

        document.getElementById("confirmvaluecountsubtotal").innerText = document.getElementById("valuecountsubtotal").innerText;
        document.getElementById("confirmtax").innerText = document.getElementById("tax").innerText;
        document.getElementById("confirmgrandtotal").innerText = document.getElementById("grandtotal").innerText;

        document.getElementById("confirmyourname").innerText = document.getElementById("yourname").value;
        document.getElementById("confirmemail").innerText = document.getElementById("email").value;
        document.getElementById("confirmphone").innerText = document.getElementById("phone").value;
        document.getElementById("confirmaddress").innerText = document.getElementById("address").value;
        document.getElementById("confirmcity").innerText = document.getElementById("city").value;
        document.getElementById("confirmarea").innerText = document.getElementById("area").value;
        document.getElementById("confirmpostcode").innerText = document.getElementById("postcode").value;
        document.getElementById("confirmordernote").innerText = document.getElementById("ordernote").value;
    }

</script>


<script>

      function purchase() {
        var room = $("#valuecountroom");
        var bath = $("#valuecountbath");

        var kitchen = $("#valuecountkitchen");
        var fridge = $("#valuecountfridge");
        var garden = $("#valuecountgarden");

        var roomprice = parseInt(room.html());
        var bathprice = parseInt(bath.html());

        var kitchenprice = parseInt(kitchen.html());
        var fridgeprice = parseInt(fridge.html());
        var gardenprice = parseInt(garden.html());

        var roomtotal = roomprice+bathprice;
        var extratotal = kitchenprice+fridgeprice+gardenprice;

        var subtotal = roomtotal+extratotal;

        document.getElementById("valuecountall").innerText = roomtotal;
        document.getElementById("valuecountsubtotal").innerText = subtotal;

        var tax = subtotal*15;
        var subtax = tax/100;
        document.getElementById("tax").innerText = subtax;

        var grandtotal = subtotal-subtax;
        document.getElementById("grandtotal").innerText = grandtotal;

        var data = $('#grandtotal').html();
        $('#totalprice').val(data);

      }

//       function myFunction() {
//   // Get the checkbox
//   var checkBox = document.getElementById("myCheck");
//   // Get the output text
//   var text = document.getElementById("text");

//   // If the checkbox is checked, display the output text
//   if (checkBox.checked == true){
//     text.style.display = "block";
//   } else {
//     text.style.display = "none";
//   }
// }

    

    function roomNum()
    {
        document.getElementById("roomnum").innerText = document.getElementById("inputroom").value;
        document.getElementById("roomcount").innerText = document.getElementById("inputroom").value;

        var roomnum = parseInt(document.getElementById('inputroom').value, 10);
        var roomPrice = roomnum*30;
        document.getElementById("valuecountroom").innerText = roomPrice;

         purchase();
        
    }

    function bathroomNum()
    {
        document.getElementById("bathroomnum").innerText = document.getElementById("inputbath").value;
        document.getElementById("bathcount").innerText = document.getElementById("inputbath").value;

        var bathnum = parseInt(document.getElementById('inputbath').value, 10);
        var bathPrice = bathnum*20;
        document.getElementById("valuecountbath").innerText = bathPrice;

        purchase();
    }

    function kitchenNum()
    {
        document.getElementById("kitchencount").innerText = document.getElementById("inputkitchen").value;

        var kitchennum = parseInt(document.getElementById('inputkitchen').value, 10);
        var kitchenPrice = kitchennum*50;
        document.getElementById("valuecountkitchen").innerText = kitchenPrice;

        purchase();
    }

    function fridgeNum()
    {
        document.getElementById("fridgecount").innerText = document.getElementById("inputfridge").value;

        var fridgenum = parseInt(document.getElementById('inputfridge').value, 10);
        var fridgePrice = fridgenum*50;
        document.getElementById("valuecountfridge").innerText = fridgePrice;

        purchase();
    }

    function gardenNum()
    {
        document.getElementById("gardencount").innerText = document.getElementById("inputgarden").value;

        var gardennum = parseInt(document.getElementById('inputgarden').value, 10);
        var gardenPrice = gardennum*50;
        document.getElementById("valuecountgarden").innerText = gardenPrice;

        purchase();
    }
</script>

<script>
    if($('.loc1').hasClass('active')){
        var data = $('#location1').html();
        $('#total').val(data);
        }
    function makeActive() {
       var element2 = document.getElementById("loc2");
       element2.classList.add("active");
       if($('.loc2').hasClass('active')){
        var data = $('#location2').html();
        $('#total').val(data);
        }
    }

    function makeActive2() {
       var element3 = document.getElementById("loc3");
       element3.classList.add("active");
       if($('.loc3').hasClass('active')){
        var data = $('#location3').html();
        $('#total').val(data);
        }
    }

    function makeActive3() {
       var element4 = document.getElementById("loc4");
       element4.classList.add("active");
       if($('.loc4').hasClass('active')){
        var data = $('#location4').html();
        $('#total').val(data);
        }
    }

    function makeActive4() {
       var element5 = document.getElementById("loc5");
       element5.classList.add("active");
       if($('.loc5').hasClass('active')){
        var data = $('#location5').html();
        $('#total').val(data);
        }
    }

    
</script>

<script>

     if($('.dateselect').hasClass('active')){
        var date = $('#selectdate').html();
        $('#date').val(date);
     }

    function selectDate1()
    {
       var date1 = document.getElementById("dateselect1");
       date1.classList.add("active");
       if($('.dateselect1').hasClass('active')){
        var date = $('#selectdate1').html();
        //var date = \Carbon\Carbon::parse("06 dec,2021");
        $('#date').val(date);
        }
    }

    function selectDate2()
    {
       var date2 = document.getElementById("dateselect2");
       date2.classList.add("active");
       if($('.dateselect2').hasClass('active')){
        var date = $('#selectdate2').html();
        //var date = \Carbon\Carbon::parse("06 dec,2021");
        $('#date').val(date);
        }
    }

    function selectDate3()
    {
       var date3 = document.getElementById("dateselect3");
       date3.classList.add("active");
       if($('.dateselect3').hasClass('active')){
        var date = $('#selectdate3').html();
        //var date = \Carbon\Carbon::parse("06 dec,2021");
        $('#date').val(date);
        }
    }

    function selectDate4()
    {
       var date4 = document.getElementById("dateselect4");
       date4.classList.add("active");
       if($('.dateselect4').hasClass('active')){
        var date = $('#selectdate4').html();
        //var date = \Carbon\Carbon::parse("06 dec,2021");
        $('#date').val(date);
        }
    }
</script>

<script>

    if($('.timeselect').hasClass('active')){
        var time = $('#selecttime').html();
        $('#time').val(time);
     }

    function selectTime1()
    {
       var time1 = document.getElementById("timeselect1");
       time1.classList.add("active");
       if($('.timeselect1').hasClass('active')){
        var time = $('#selecttime1').html();
        //var date = \Carbon\Carbon::parse("06 dec,2021");
        $('#time').val(time);
        }
    }

    function selectTime2()
    {
       var time2 = document.getElementById("timeselect2");
       time2.classList.add("active");
       if($('.timeselect2').hasClass('active')){
        var time = $('#selecttime2').html();
        //var date = \Carbon\Carbon::parse("06 dec,2021");
        $('#time').val(time);
        }
    }

    function selectTime3()
    {
       var time3 = document.getElementById("timeselect3");
       time3.classList.add("active");
       if($('.timeselect3').hasClass('active')){
        var time = $('#selecttime3').html();
        //var date = \Carbon\Carbon::parse("06 dec,2021");
        $('#time').val(time);
        }
    }
    
</script>

{{-- <script>
    if($('.hem').hasClass('active')){
        alert('d');
        var data = $('#location1').html();
        $('#total1').val(data);
        }
//     var data = $('#location').html();
// $('#total').val(data);
    </script> --}}

</html>