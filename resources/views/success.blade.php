<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>QIXER - Service Based Business HTML Template</title>

    <!-- favicon -->
    <link rel=icon href="favicons.ico" sizes="16x16" type="image/icon">
    <!-- animate -->
    <link rel="stylesheet" href="assets/css/animate.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <!-- LineAwesome -->
    <link rel="stylesheet" href="assets/css/line-awesome.min.css">
    <!-- fontawesome -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <!-- Nice Select -->
    <link rel="stylesheet" href="assets/css/nice-select.css">
    <!-- Main Stylesheet -->
    <link rel="stylesheet" href="assets/css/style.css">

</head>

<body>
    <!-- preloader area start -->
    <div class="preloader" id="preloader">
        <div class="preloader-inner">
            <div class="loader">
                <div></div>
                <div></div>
                <div></div>
                <div></div>
            </div>
        </div>
    </div>
    <!-- preloader area end -->
    <!-- Header area Starts -->
    <header class="header-style-01">
        <!-- Menu area Starts -->
        <nav class="navbar navbar-area navbar-two navbar-expand-lg navbar-bg-1">
            <div class="container container-two nav-container">
                <div class="responsive-mobile-menu">
                    <div class="logo-wrapper">
                        <a href="multistep_form.html" class="logo">
                            <img src="assets/img/logo-02.png" alt="">
                        </a>
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bizcoxx_main_menu" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bizcoxx_main_menu">
                    <ul class="navbar-nav">
                        <li><a href="#0">Home</a></li>
                        <li><a href="#0">About</a></li>
                        <li><a href="#0">Service List</a></li>
                        <li><a href="#0">Dashboard</a></li>
                        <li><a href="multistep_form.html">multistep Form</a></li>
                        <li><a href="#0">Blog</a></li>
                        <li><a href="#0">contact</a></li>
                    </ul>
                </div>
                <div class="nav-right-content">
                    <ul>
                        <li>
                            <a href="#">
                                <div class="info-bar-item">
                                    <div class="cart-icon icon">
                                        <i class="las la-shopping-cart"></i>
                                        <div class="cart-list style-02">
                                            <span class="single-list"> Cart One </span>
                                            <span class="single-list"> Cart Two </span>
                                            <span class="single-list"> Cart Three </span>
                                            <span class="single-list"> Cart Four </span>
                                            <span class="single-list"> Cart Five </span>
                                        </div>
                                    </div>
                                    <div class="notification-icon icon">
                                        <i class="las la-bell"></i>
                                        <span class="notification-number style-02"> 4 </span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="info-bar-item-two">
                                    <div class="author-thumb">
                                        <img src="assets/img/author-nav.jpg" alt="">
                                    </div>
                                    <div class="author-nav-content">
                                        <span class="title"> Alex Jerin </span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Menu area end -->
    </header>
    <!-- Header area end -->
    
                        <!-- Successful Complete -->
                        <fieldset class="padding-top-80 padding-bottom-100">
                            <div class="form-card successful-card">
                                <h2 class="title-step"> SUCCESS ! </h2>
                                <div class="succcess-icon">
                                    <i class="las la-check"></i>
                                </div>
                                <h5 class="purple-text text-center">You Have Successfully Signed Up</h5>
                                <div class="btn-wrapper text-center margin-top-35">
                                    <a href="{{route('home')}}" class="cmn-btn btn-bg-1"> Back To Home</a>
                                </div>
                            </div>
                        </fieldset>
    <!-- Location Overview area end -->
    <!-- footer area start -->
    <footer class="footer-area section-bg-2">
        <div class="footer-top padding-top-100 padding-bottom-70">
            <div class="container container-two">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget widget style-02">
                            <div class="about_us_widget">
                                <a href="multistep_form.html" class="footer-logo"> <img src="assets/img/logo-02.png" alt="footer logo"></a>
                            </div>
                            <div class="footer-inner">
                                <p class="footer-para">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget widget style-02">
                            <h6 class="widget-title">Category</h6>
                            <div class="footer-inner">
                                <ul class="footer-link-list">
                                    <li class="list"><a href="#">Cleaning</a></li>
                                    <li class="list"><a href="#">House Move</a></li>
                                    <li class="list"><a href="#">Electric</a></li>
                                    <li class="list"><a href="#">Painting</a></li>
                                    <li class="list"><a href="#">Salon & Spa</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget widget style-02">
                            <h6 class="widget-title">Store Info</h6>
                            <div class="footer-inner">
                                <ul class="footer-link-address">
                                    <li class="list"><span class="address"> <i class="las la-map-marker-alt"></i> 41/1, Hilton Mall, NY City </span></li>
                                    <li class="list"> <span class="address"> <a href="tel:+012-78901234"> <i class="las la-mobile-alt"></i> +012-78901234</a> </span></li>
                                    <li class="list"> <span class="address"> <a href="mailto:help@mail.com"> <i class="las la-envelope"></i> help@mail.com</a> </span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="footer-widget widget style-02">
                            <h6 class="widget-title">Get In Touch</h6>
                            <div class="footer-inner">
                                <p class="subscribe-para"> Sign up to our mailing list now! </p>
                                <form class="subscribe-form" action="#">
                                    <div class="widget-form-single">
                                        <input class="form--control" type="text" name="email" placeholder="Your mail here">
                                        <button type="submit"> <i class="lab la-telegram-plane"></i> </button>
                                    </div>
                                </form>
                                <div class="footer-socials style-02">
                                    <ul class="footer-social-list">
                                        <li class="lists">
                                            <a class="facebook" href="#0"> <i class="lab la-facebook-f"></i> </a>
                                        </li>
                                        <li class="lists">
                                            <a class="twitter" href="#0"> <i class="lab la-twitter"></i> </a>
                                        </li>
                                        <li class="lists">
                                            <a class="instagram" href="#0"> <i class="lab la-instagram"></i> </a>
                                        </li>
                                        <li class="lists">
                                            <a class="youtube" href="#0"> <i class="lab la-youtube"></i> </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright-area style-02 copyright-border">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-6">
                        <ul class="copyright-list">
                            <li class="list">
                                <a href="#0"> Privacy Policy </a>
                            </li>
                            <li class="list">
                                <a href="#0"> Terms & Conditions </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="copyright-contents">
                            <span> All copyright (C) 2021 Reserved </span>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="copyright-payment">
                            <ul class="payment-list">
                                <li class="list">
                                    <a href="#0"> <img src="assets/img/footer/c1.png" alt=""> </a>
                                </li>
                                <li class="list">
                                    <a href="#0"> <img src="assets/img/footer/c2.png" alt=""> </a>
                                </li>
                                <li class="list">
                                    <a href="#0"> <img src="assets/img/footer/c3.png" alt=""> </a>
                                </li>
                                <li class="list">
                                    <a href="#0"> <img src="assets/img/footer/c4.png" alt=""> </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- footer area end -->

    <!-- back to top area start -->
    <div class="back-to-top">
        <span class="back-top"><i class="fa fa-angle-up"></i></span>
    </div>
    <!-- back to top area end -->



    <!-- jquery -->
    <script src="assets/js/jquery-3.6.0.min.js"></script>
    <!-- jquery Migrate -->
    <script src="assets/js/jquery-migrate-1.4.1.min.js"></script>
    <!-- bootstrap -->
    <script src="assets/js/bootstrap.bundle.min.js"></script>
    <!-- wow -->
    <script src="assets/js/wow.min.js"></script>
    <!-- Nice Select -->
    <script src="assets/js/jquery.nice-select.js"></script>
    <!-- main js -->
    <script src="assets/js/main.js"></script>

</body>



</html>